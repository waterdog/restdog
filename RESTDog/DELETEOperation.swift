import Foundation
import CoreData
import WDOperations

open class DeleteEntityRESTOperation<T: RESTRemoteService>: WDOperation where T.Entity: NSManagedObject {
  let internalQueue: WDOperationQueue
  let remoteService: T
  let entity: T.Entity
  
  public init(entity: T.Entity, remoteService: T, conditions: [WDOperationCondition]) {
    self.internalQueue = WDOperationQueue()
    internalQueue.isSuspended = true
    
    self.remoteService = remoteService
    self.entity = entity
    
    super.init()
    
    conditions.forEach { self.addCondition($0) }
  }
  
  override open func execute() {
    if !isCancelled {
      let uuid = UUID().uuidString
      var cacheURL = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
      cacheURL.appendPathComponent(uuid)
      
      let request = remoteService.deleteEntity(entity)
      let task = URLSessionTaskOperation(request: request, cache: cacheURL) { response in
        if response?.statusCode == 204 {
          let context = self.entity.managedObjectContext!
          context.performAndWait {
            context.delete(self.entity)
            try? context.save()
          }
        }
      }
      internalQueue.addOperation(task)
      
      internalQueue.isSuspended = false
    }
  }
  
  override open func cancel() {
    internalQueue.cancelAllOperations()
    super.cancel()
  }
}
