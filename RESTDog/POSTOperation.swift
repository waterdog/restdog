import Foundation
import CoreData
import WDOperations

open class POSTEntityRESTOperation<T: RESTRemoteService>: WDOperation where T.Entity: NSManagedObject {
  let entity: T.Entity
  let remoteService: T
  let context: NSManagedObjectContext
  let parser: JSONParserHandler
  let internalQueue: WDOperationQueue
  let handler: (Bool) -> Void
  private var successInCreatingEntity = false
  
  public init(remoteService: T, entity: T.Entity, context: NSManagedObjectContext, parser: JSONParserHandler, conditions: [WDOperationCondition], handler: @escaping (Bool) -> Void) {
    self.remoteService = remoteService
    self.entity = entity
    self.context = context
    self.parser = parser
    self.handler = handler
    
    self.internalQueue = WDOperationQueue()
    internalQueue.isSuspended = true
    
    super.init()
    
    conditions.forEach { self.addCondition($0) }
  }
  
  override open func execute() {
    if !isCancelled {
      let uuid = UUID().uuidString
      var cacheURL = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
      cacheURL.appendPathComponent(uuid)
      
      let requestOperation = URLSessionTaskOperation(
        request: remoteService.postEntity(entity),
        cache: cacheURL
      )
      internalQueue.addOperation(requestOperation)
      
      let parseOperation = ParseJSONOperation(cache: cacheURL, context: context, parser: parser) { success in
        self.successInCreatingEntity = success
      }
      parseOperation.addDependency(requestOperation)
      internalQueue.addOperation(parseOperation)
      
      parseOperation.completionBlock = {
        self.handler(self.successInCreatingEntity)
        self.finish()
      }
      
      internalQueue.isSuspended = false
    }
  }
  
  override open func cancel() {
    internalQueue.cancelAllOperations()
    super.cancel()
  }
}
