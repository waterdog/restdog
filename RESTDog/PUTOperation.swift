import Foundation
import CoreData
import WDOperations

open class PUTOperation<T: RESTRemoteService>: WDOperation where T.Entity: NSManagedObject {
  let entity: T.Entity
  let remoteService: T
  let parser: JSONParserHandler
  let operationQueue = WDOperationQueue()
  let handler: (Bool) -> Void
  
  public init(entity: T.Entity, remoteService: T, parser: JSONParserHandler, conditions: [WDOperationCondition], handler: @escaping (Bool) -> Void) {
    self.entity = entity
    self.remoteService = remoteService
    self.parser = parser
    self.handler = handler
    
    super.init()
    
    operationQueue.isSuspended = true
    conditions.forEach { self.addCondition($0) }
  }
  
  override open func execute() {
    guard !isCancelled else {
      return
    }
    
    let cache = try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
      .appendingPathComponent(UUID().uuidString)
    
    let request = remoteService.putEntity(entity)
    let taskOperation = URLSessionTaskOperation(request: request, cache: cache)
    
    let parseOperation = ParseJSONOperation(cache: cache, context: entity.managedObjectContext!, parser: parser) { success in
      self.handler(success)
      self.finish()
    }
    parseOperation.addDependency(taskOperation)
    
    operationQueue.addOperation(taskOperation)
    operationQueue.addOperation(parseOperation)
    
    operationQueue.isSuspended = false
  }
  
  override open func cancel() {
    operationQueue.cancelAllOperations()
    super.cancel()
  }
}
