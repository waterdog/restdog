import Foundation
import CoreData
import WDOperations

final public class ParseJSONOperation: WDOperation {
  
  let cache: URL
  let context: NSManagedObjectContext
  let parser: JSONParserHandler
  let completionHandler: (Bool) -> Void
  
  init(cache: URL, context: NSManagedObjectContext, parser: JSONParserHandler, handler: @escaping (Bool) -> Void) {
    self.cache = cache
    self.context = context
    self.parser = parser
    self.completionHandler = handler
  }
  
  override public func execute() {
    
    defer {
      if !isCancelled {
        self.finish()
      }
    }
    
    do {
      let cachedData = try Data(contentsOf: cache)
      parser.parse(jsonData: cachedData, in: context, handler: completionHandler)
    } catch {
      return
    }
  }
}
