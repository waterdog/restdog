import Foundation
import CoreData

public typealias JSONDictionary = [String: Any?]

public protocol RemoteServiceIdentifiable {
  var remoteID: Int32 { get set }
  static func make(fromJSON json: JSONDictionary, in context: NSManagedObjectContext) -> Self?
}

public protocol RESTEntity: RemoteServiceIdentifiable {
  static var urlEntityName: String { get }
  var json: JSONDictionary { get }
}
