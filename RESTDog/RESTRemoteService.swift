import Foundation

public protocol RESTRemoteService {
  associatedtype Entity: RESTEntity
  var baseURL: URL { get }
  
  func getAll(page: Int?) -> URLRequest
  func getNextPageNumber(from response: HTTPURLResponse) -> Int?
  
  func getEntity(withRemoteID remoteID: Int) -> URLRequest
  func postEntity(_ entity: Entity) -> URLRequest
  func putEntity(_ entity: Entity) -> URLRequest
  func deleteEntity(_ entity: Entity) -> URLRequest
}
