//
//  RemoteEntity.swift
//  RESTDog
//
//  Created by Jorge Galrito on 08/07/2018.
//  Copyright © 2018 WATERDOG mobile. All rights reserved.
//

import Foundation
import CoreData

public protocol RemoteIdentifiable {
  var remoteIdentifier: Int32 { get }
}

public typealias RemoteEntityConvertible = RemoteIdentifiable & Codable

public protocol CoreDataCodable {
}

extension NSManagedObject: CoreDataCodable {
}

extension Array: CoreDataCodable where Element: NSManagedObject {
}
