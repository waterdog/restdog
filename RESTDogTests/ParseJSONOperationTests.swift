import XCTest
import WDOperations
import CoreData
@testable import RESTDog

class TestParseHandler: JSONParserHandler {
  var expectation: XCTestExpectation?
  
  func parse(jsonData: Data, in context: NSManagedObjectContext, handler: @escaping (Bool) -> Void) {
    expectation?.fulfill()
  }
}

class ParseJSONOperationTests: XCTestCase {
  var parseHandler: TestParseHandler!
  var operationQueue: WDOperationQueue!
  var operation: Operation!
  let cacheURL: URL = {
    return try! FileManager.default.url(for: .cachesDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
      .appendingPathComponent("testParseHandlerJSON")
  }()
  
  override func setUp() {
    super.setUp()

    let persistentContainer = TestsHelper.makeCoreDataTestingPersistentContainer()
    let context = persistentContainer.newBackgroundContext()

    self.parseHandler = TestParseHandler()
    self.operation = ParseJSONOperation(
      cache: cacheURL,
      context: context,
      parser: parseHandler,
      handler: {_ in }
    )
    self.operationQueue = WDOperationQueue()
  }
  
  func testOperationCanBeCreated() {
    let expectation = XCTestExpectation(description: "Parse handler should have been called")
    parseHandler.expectation = expectation
    XCTAssertNotNil(operation)
  }
  
  func testParseHandlerIsCalled() {
    // Construct the cache data
    let json = ["key": "value"]
    let jsonData = try! JSONSerialization.data(withJSONObject: json, options: [])
    try! jsonData.write(to: cacheURL)
    
    let expectation = XCTestExpectation(description: "Parse handler should have been called")
    parseHandler.expectation = expectation
    
    operationQueue.addOperation(operation)
    wait(for: [expectation], timeout: 1)
    
    XCTAssertTrue(operation.isFinished)
  }
}
