import Foundation
import CoreData

struct TestsHelper {
  static func makeCoreDataTestingPersistentContainer() -> NSPersistentContainer {
    let semaphore = DispatchSemaphore(value: 1)
    let bundle = Bundle(for: ParseJSONOperationTests.self)
    let model = NSManagedObjectModel.mergedModel(from: [bundle])!
    let container = NSPersistentContainer(name: "TestingModel", managedObjectModel: model)
    let storeDescription = NSPersistentStoreDescription()
    storeDescription.type = NSInMemoryStoreType
    container.persistentStoreDescriptions = [storeDescription]
    container.loadPersistentStores(completionHandler: { _,_ in semaphore.signal() })
    semaphore.wait()
    return container
  }
}
